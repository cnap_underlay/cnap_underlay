FROM ubuntu:xenial

# Update apt
RUN apt-get update --fix-missing
RUN apt-get install -qqy apt-utils
RUN apt-get dist-upgrade -y

# Install some standard stuff
RUN apt-get install -qqy sudo net-tools htop
RUN apt-get install -qqy ssh openssh-server
RUN apt-get install -qqy git gitg
RUN apt-get install -qqy emacs gedit
RUN apt-get install -qqy udev
RUN apt-get install -y bash-completion usbutils

## Install pybombs
RUN apt-get install -y python-apt python-pip
RUN pip install --upgrade pip
RUN pip install pybombs

# Create new user and set password
RUN useradd -ms /bin/bash cnap
RUN echo 'cnap:cnap' | chpasswd
RUN echo 'root:cnap' | chpasswd
RUN usermod -aG sudo cnap
RUN echo "cnap ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/cnap
RUN chown -R cnap:cnap /home/cnap

# Set up X11 configuration
RUN apt-get install -qqy x11-apps
RUN sudo echo "X11UseLocalhost no" >> /etc/ssh/sshd_config


# Build GNU Radio
USER cnap
WORKDIR /home/cnap
RUN pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
RUN mkdir prefix
RUN pybombs prefix init /home/cnap/prefix -a prefix

RUN pybombs install uhd
RUN pybombs install gnuradio

# Add limesdr packages
RUN pybombs install soapysdr limesuite gr-osmosdr

USER root
RUN ldconfig


USER cnap
RUN echo "source /home/cnap/prefix/setup_env.sh" >> /home/cnap/.bashrc




USER root
EXPOSE 22
ENTRYPOINT service ssh restart && chown -R cnap:cnap /home/cnap/working && /bin/bash
